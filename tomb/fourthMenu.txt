购墓未付款提醒-生成通知短信|001001001
购墓未付款提醒-确认发送短信
购墓未付款提醒-查看业务档案
购墓未付款提醒-付款
购墓未付款提醒-回访登记
维护费到期提醒-生成通知短信|001001002
维护费到期提醒-确认发送短信
维护费到期提醒-缴维护费
预约落葬到期提醒-生成通知短信|001001003
预约落葬到期提醒-确认发送短信
工单验收到期提醒-生成通知短信|001001004
工单验收到期提醒-确认发送短信
工单验收到期提醒-打印施工单
定金到期提醒-生成通知短信|001001005
定金到期提醒-确认发送短信
定金到期提醒-查看业务档案
定金到期提醒-延期操作
寄存超期提醒-生成通知短信|001001006
寄存超期提醒-确认发送短信
盆花超期提醒-生成通知短信|001001007
盆花超期提醒-确认发送短信
落葬使用人生日提醒-确认发送短信|001001008
落葬使用人忌日提醒-确认发送短信|001001009
瓷像验收与安装提醒-生成通知短信|001001010
瓷像验收与安装提醒-确认发送短信
投诉处理提醒-生成通知短信|001001011
投诉处理提醒-确认发送短信
落葬未葬前检查提醒-生成通知短信|001001012
落葬未葬前检查提醒-确认发送短信
业务未结账提醒-生成通知短信|001001013
业务未结账提醒-确认发送短信
业务未结账提醒-付款
碑文未发单提醒-发单|001001014
碑文未发单提醒-打印碑文登记单
瓷像未完成提醒-生成通知短信|001001015
瓷像未完成提醒-确认发送短信
瓷像未安装提醒-生成通知短信|001001016
瓷像未安装提醒-确认发送短信
墓穴保留到期提醒-生成通知短信|001001017
墓穴保留到期提醒-确认发送短信
售前客户-新客户登记|002001001
售前客户-再访登记
售前客户-绑定业务
售前客户-解除绑定
看墓接待-看墓状态|002001002
客户跟进-客户跟进|002001003
客户跟进-绑定业务
客户跟进-解除绑定
预定保留-新增预定保留|002002002
预定保留-转为保留
预定保留-进入洽谈
墓穴保留-新增保留|002002003
墓穴保留-进入洽谈
墓穴保留-查看
墓穴保留-打印留墓单
内部保留-新增保留|002002005
现场划地-新增墓穴|002003001
现场划地-修改墓穴
现场划地-墓穴上帐
现场划地-上传文件
现场划地-查看墓穴
现场划地-业务办理
现场墓穴管理-删除墓穴|002003002
现场墓穴管理-撤销上账
现场墓穴管理-添加行列
现场墓穴管理-删除整行
现场墓穴管理-删除整列
现场墓穴管理-上传文件
现场墓穴管理-查看墓穴
现场墓穴管理-业务办理
业务登记-新业务登记|003001001
业务登记-打印
业务登记-业务洽谈
业务登记-碑文洽谈
业务登记-售后登记
业务登记-更多...
业务洽谈-新业务登记|003001002
业务洽谈-打印
业务洽谈-业务洽谈
业务洽谈-碑文洽谈
业务洽谈-售后登记
业务洽谈-更多...
合同管理-打印墓穴购销合同|003001004
合同管理-打印合同补充协议
定金延期-延期操作|003001005
碑文管理-打印碑文登记单|003002001
碑文校对-加急发单|003002002
碑文模板-新增|003002003
售后管理-注销|003003001
售后管理-打印
售后管理-内部保修
售后发单-发单|003003002
售后发单-批量发单
退墓洽谈-退墓登记|003004001
退墓管理-换墓洽谈|003004002
退墓管理-退(换)墓单
迁葬洽谈-迁葬登记|003004003
迁墓管理-换墓洽谈|003004004
墓穴设计管理-查看流程图|004001002
墓穴设计管理-设计确认单
墓穴设计流程配置-保存|004003001
瓷像管理-瓷像业务登记单|005001002
瓷像管理-瓷像业务订单袋
瓷像发单-瓷像业务订单袋|005002001
瓷像发单-转至批量发单列表
瓷像安装-瓷像业务订单袋|005003001
瓷像安装-转至批量安装列表
瓷像校对-瓷像业务订单袋|005003002
瓷像校对-转至批量校对列表
已预约落葬列表-预约落葬单|006002001
已预约落葬列表-墓穴证书使用人
已预约落葬列表-查看落葬详情
葬前检查-查看落葬详情|006002002
葬前检查-批量检查
落葬登记-批量落葬|006002003
落葬登记-实际落葬单
落葬管理-安葬档案袋|006002004
礼仪服务预定列表-礼仪服务确认单|006003001
接待控制-新增|006004001
商品零售-新增|007001001
施工单管理-查看|008001001
施工单管理-打印施工单
施工单管理-监控
碑文工单管理-查看|008001002
碑文工单管理-打印施工单
碑文工单管理-监控
瓷像工单管理-瓷像业务订单袋|008001003
瓷像工单管理-转至批量发单列表
礼仪服务工单管理-监控|008001004
佛事工单管理-监控|008001005
退墓改建工单管理-查看|008001006
退墓改建工单管理-监控
售后服务工单管理-查看|008001007
售后服务工单管理-打印施工单
售后服务工单管理-监控
墓穴保修工单管理-查看|008001008
墓穴保修工单管理-打印施工单
墓穴保修工单管理-监控
派单管理-查看|008002001
派单管理-打印施工单
派单管理-打印总交代单
派单管理-转至批量验收列表
售后服务派单-查看|008002002
售后服务派单-打印施工单
售后服务派单-打印总交代单
售后服务派单-转至批量验收列表
墓穴报修派单-查看|008002003
墓穴报修派单-打印施工单
墓穴报修派单-打印总交代单
墓穴报修派单-转至批量验收列表
工程验收-查看|008003001
工程验收-转至批量验收列表
碑文工单验收-查看|008003002
碑文工单验收-转至批量验收列表
瓷像工单验收-瓷像业务订单袋|008003003
瓷像工单验收-转至批量发单列表
礼仪服务工单验收-查看|008003004
礼仪服务工单验收-转至批量验收列表
佛事服务工单验收-查看|008003005
佛事服务工单验收-转至批量验收
退墓改建工单验收-查看|008003006
退墓改建工单验收-转至批量验收列表
售后服务工单验收-查看|008003007
售后服务工单验收-转至批量验收列表
墓穴报修工单验收-查看|008003008
墓穴报修工单验收-转至批量验收列表
施工情况查看-查看工单信息|008004001
通知客户验收-查看|008004002
通知客户验收-转至批量验收列表
客户验收-查看|008004003
客户验收-客户验收通知单
客户验收-客户验收确认单
客户验收-转至批量验收列表
客户验收问题-查看|008004004
退单处理-查看|008005001
墓型管理-新增|009001001
墓区管理-新增子墓区|009002001
墓区管理-修改当前墓区
墓区管理-删除当前墓区
墓穴管理-新增墓穴|009002002
墓穴上账-墓穴上账|009003001
墓穴上账-绑定项目
墓穴上账-墓穴价格批量导出
墓穴上账-批量设置价格导入
墓穴上账-查看上账
墓穴上账-撤销上账
墓穴上账-撤销绑定项目
调价管理-查看|009004002
墓区巡视-现场巡视|009005001
投诉登记-登记|010001001
投诉修改-修改|010001002
投诉派发-派发|010001003
投诉处理-处理|010001004
处理管理-处理|010001005
投诉反馈-反馈|010001006
申请人管理-已购墓客户管理|010002001
补办墓穴证-补办墓穴证|010002003
补办墓穴证-打印墓穴证
锁定/提醒-锁定|010003001
锁定/提醒-提醒
锁定管理-修改|010003002
锁定管理-解锁
提醒管理-修改|010003003
墓穴档案-墓穴档案|011004002
短信模板-新增|011007001
短信发送查询-确认发送|011007002
财务结算-保存|012001001
财务结算-刷新
财务结算-转至财务发票
退墓结算-保存|012001002
退墓结算-刷新
退墓结算-转至退墓发票
迁葬结算-保存|012001003
迁葬结算-刷新
迁葬结算-转至迁葬发票
缴维护费-维护费管理|012002002
财务发票-保存|012003001
财务发票-刷新
财务发票-转至财务结算
退墓发票-保存|012003002
退墓发票-刷新
退墓发票-转至退墓结算
迁葬发票-保存|012003003
迁葬发票-刷新
迁葬发票-转至迁葬结算
派发票据-派发票据|012004001
派发票据-修改使用范围
派发票据-删除派发
轧帐管理-新增轧账|012005001
零售结算-保存|012006001
零售结算-刷新
零售结算-转至财务发票
墓穴葬式-新增|013001001
石棺摆放方式-新增|013001002
礼仪服务类型-新增|013001003
服务类型-新增|013002001
服务项目-新增|013002002
服务项目-批量修改
墓穴固定费用-新增|013002003
瓷像项目配置-新增|013002004
服务班组-新增|013002005
服务班组人员-新增|013002006
营销点管理-新增项目|013003001
营销人管理-新增|013003002
代理人员管理-新增|013003004
营销人权限归属-刷新|013003005
营销人权限归属-保存
营销人权限归属-查看列表
优惠授权管理-新增|013004001
代码维护-新增|013005001
代码维护-修改
代码维护-删除
重大业务配置-保存|013005002
系统配置-保存|013005003
系统消息节点配置|013005004