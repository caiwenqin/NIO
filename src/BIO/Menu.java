package BIO;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Menu {
    public static void main(String[] args) {
       new Menu().firstMenu();
       new Menu().secondMenu();
       new Menu().thirdMenu();
       new Menu().fourthMenu();
    }

    public void firstMenu() {
        String sql = "INSERT INTO `t_sys_menu_basic`(menu_code,up_menu_code,menu_name,menu_desc,menu_type,menu_url,menu_icon,model_type,permission,update_time,update_op_id,update_op_name,create_time,create_op_id,create_op_name,is_active) VALUES ( '%s', 'CEM000000000000', '%s', '%s', '0', '', '', '2', '', '0', '0', '', '0', '0', '', '1');";
        String inputPath = "F:/tomb/firstMenu.txt";
        String outPath = "F:/tomb/sql/firstMenuSql.txt";
        checkFile(inputPath);
        List<String> list = readFile(inputPath);
        List<String> sqlList = new ArrayList<>();
        for (int i = 0; i<list.size(); i++){
            sqlList.add(String.format(sql,"CEM0"+String.format("%02d",i+1)+"000000000",list.get(i),list.get(i)));
        }
        checkFile(outPath);
        writeFile(sqlList,outPath);
    }

    public void secondMenu(){
        String sql = "INSERT INTO `t_sys_menu_basic`(menu_code,up_menu_code,menu_name,menu_desc,menu_type,menu_url,menu_icon,model_type,permission,update_time,update_op_id,update_op_name,create_time,create_op_id,create_op_name,is_active) VALUES ( '%s', '%s', '%s', '%s', 0, 'XXXX', '', '2', '', '0', '0', '', '0', '0', '', '1');";
        String inputPath = "F:/tomb/secondMenu.txt";
        String outPath = "F:/tomb/sql/secondMenuSql.txt";
        checkFile(inputPath);
        List<String> list = readFile(inputPath);
        List<String> sqlList = new ArrayList<>();
        String code = null;
        int count = 1;
        for (String s : list) {
            String[] params = s.split("\\|");
            if (params.length==2) {
                code = params[1];
                count = 1;
            } else {
                count++;
            }
            String name = s.split("\\|")[0];
            sqlList.add(String.format(sql, "CEM" + code + String.format("%03d", count) + "000000", "CEM" + code + "000000000", name, name));
        }
        checkFile(outPath);
        writeFile(sqlList,outPath);
    }

    public void thirdMenu(){
        String sql = "INSERT INTO `t_sys_menu_basic`(menu_code,up_menu_code,menu_name,menu_desc,menu_type,menu_url,menu_icon,model_type,permission,update_time,update_op_id,update_op_name,create_time,create_op_id,create_op_name,is_active) VALUES ( '%s', '%s', '%s', '%s', 1, 'XXXX', '', '2', '', '0', '0', '', '0', '0', '', '1');";
        String inputPath = "F:/tomb/thirdMenu.txt";
        String outPath = "F:/tomb/sql/thirdMenuSql.txt";
        checkFile(inputPath);
        List<String> list = readFile(inputPath);
        List<String> sqlList = new ArrayList<>();
        String code = null;
        int count = 1;
        for (String s : list) {
            String[] params = s.split("\\|");
            if (params.length==2) {
                code = params[1];
                count = 1;
            } else {
                count++;
            }
            String name = s.split("\\|")[0];
            sqlList.add(String.format(sql, "CEM" + code + String.format("%03d", count) + "000", "CEM" + code + "000000", name, name));
        }
        checkFile(outPath);
        writeFile(sqlList,outPath);
    }

    public void fourthMenu(){
        String sql = "INSERT INTO `t_sys_menu_basic`(menu_code,up_menu_code,menu_name,menu_desc,menu_type,menu_url,menu_icon,model_type,permission,update_time,update_op_id,update_op_name,create_time,create_op_id,create_op_name,is_active) VALUES ( '%s', '%s', '%s', '%s', 2, 'XXXX', '', '2', '', '0', '0', '', '0', '0', '', '1');";
        String inputPath = "F:/tomb/fourthMenu.txt";
        String outPath = "F:/tomb/sql/fourthMenuSql.txt";
        checkFile(inputPath);
        List<String> list = readFile(inputPath);
        List<String> sqlList = new ArrayList<>();
        String code = null;
        int count = 1;
        for (String s : list) {
            String[] params = s.split("\\|");
            if (params.length==2) {
                code = params[1];
                count = 1;
            } else {
                count++;
            }
            String name = s.split("\\|")[0];
            assert code != null;
            sqlList.add(String.format(sql, "CEM" + code + String.format("%03d", count), "CEM" + code.substring(0,9) + "000", name, name));
        }
        checkFile(outPath);
        writeFile(sqlList,outPath);
    }

    public static List<String> readFile(String path) {
        BufferedReader reader = null;
        String content;
        List<String> list = new ArrayList<>();
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(path),"GBK"));
            while ((content = reader.readLine()) != null) {
                list.add(content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public static void writeFile(List<String> content,String path){

        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(path), "GBK"))) {
            for (String line : content) {
                writer.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void checkFile(String path){
        File file=new File(path);
        boolean result = false;
        if(!file.exists()){
            try {
                result = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}